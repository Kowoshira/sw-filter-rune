import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bj5Component } from './bj5.component';

describe('Bj5Component', () => {
  let component: Bj5Component;
  let fixture: ComponentFixture<Bj5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bj5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bj5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
