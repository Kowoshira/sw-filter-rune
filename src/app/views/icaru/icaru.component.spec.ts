import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IcaruComponent } from './icaru.component';

describe('IcaruComponent', () => {
  let component: IcaruComponent;
  let fixture: ComponentFixture<IcaruComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IcaruComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IcaruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
