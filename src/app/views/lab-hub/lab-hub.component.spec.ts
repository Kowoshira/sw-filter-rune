import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabHubComponent } from './lab-hub.component';

describe('LabHubComponent', () => {
  let component: LabHubComponent;
  let fixture: ComponentFixture<LabHubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabHubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabHubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
