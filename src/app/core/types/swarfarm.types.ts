export namespace SwarFarmApiTypes {

    export interface SkillEffect {
        name: string
        is_buff: boolean
        description: string
        icon_filename: string
    }

    export interface Skill {
        com2us_id: number
        name: string
        description: string
        slot: number
        cooltime: any
        hits: number
        passive: boolean
        aoe: boolean
        max_level: number
        level_progress_description: string
        effects: SkillEffect[]
        multiplier_formula: string
        multiplier_formula_raw: string
        icon_filename: string
    }

    export interface Monster {
        url: string
        com2us_id: number
        name: string
        image_filename: string
        element: string
        archetype: string
        attribute: string

        base_stars: number
        natural_stars: number
        can_awaken: boolean
        is_awakened: boolean
        obtainable: boolean
        awaken_bonus: any

        skills: Skill[]
        leader_skill: any
        homunculus_skills: any[]

        base_hp: number
        base_attack: number
        base_defense: number
        speed: number
        crit_damage: number
        crit_rate: number
        resistance: number
        accuracy: number
        max_lvl_hp: number
        max_lvl_attack: number
        max_lvl_defense: number

        awakens_from: {
            url: string,
            name: string,
            element: string,
        }
        awakens_to: string
        fusion_food: boolean
        homunculus: boolean
    }
}